/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2023 Marc Rautenhaus [*, previously +]
**  Copyright 2020-2023 Andreas Beckert [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef PARTIALDERIVATIVEFILTER_H
#define PARTIALDERIVATIVEFILTER_H

// standard library imports
#include <math.h>

// related third party imports
#include <QtCore>

// local application imports
#include "processingwpdatasource.h"
#include "structuredgridensemblefilter.h"
#include "structuredgrid.h"
#include "datarequest.h"

namespace Met3D
{

/**
  @brief MPartialDerivativeFilter implements partial derivative computations for gridded data.
 */
class MPartialDerivativeFilter
        : public MSingleInputProcessingWeatherPredictionDataSource
{
public:
    MPartialDerivativeFilter();

    MStructuredGrid* produceData(MDataRequest request);

    MTask* createTaskGraph(MDataRequest request);

protected:
    const QStringList locallyRequiredKeys();

private:

    /**
     * @brief computePartialDerivativeLongitude Computes the horizontal partial
     * derivative in longitudinal direction using central and at boundaries
     * forward or backward finite differences.
     * @param inputGrid pointer to the input grid.
     * @param resultGrid pointer to the result grid.
     */
    void computePartialDerivativeLongitude(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid);

    /**
     * @brief computePartialDerivativeLatitude Computes the horizontal partial
     * derivative in latitudinal direction using central and at boundaries
     * forward or backward finite differences.
     * @param inputGrid pointer to the input grid.
     * @param resultGrid pointer to the result grid.
     */
    void computePartialDerivativeLatitude(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid);

    /**
     * @brief computePartialDerivativeVertical Computes the vertical partial
     * derivative using central and at boundaries
     * forward or backward finite differences.
     * @param inputGrid pointer to the input grid.
     * @param resultGrid pointer to the result grid.
     */
    void computePartialDerivativeVertical(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid);


    /**
     * @brief computePartialDerivativePressureLongitude Computes the horizontal
     * partial derivative of pressure in longitudinal direction using central
     * and at boundaries forward or backward finite differences.
     * @param inputGrid pointer to the input grid.
     * @param resultGrid pointer to the result grid.
     */
    void computePartialDerivativePressureLongitude(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid);

    /**
     * @brief computePartialDerivativePressureLongitude Computes the horizontal
     * partial derivative of pressure in latitudinal direction using central
     * and at boundaries forward or backward finite differences.
     * @param inputGrid pointer to the input grid.
     * @param resultGrid pointer to the result grid.
     */
    void computePartialDerivativePressureLatitude(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid);

    /**
     * @brief computePressureCoordinateCorrectionLongitude Computes pressure
     * correction terms for all data on irregular vertical pressure coordinates.
     * This term corrects the derivatives on native data grid to derivatives on pressure level.
     * @param inputGrid pointer to the input grid
     * @param resultGrid pointer to the result grid
     */
    void computePressureCoordinateCorrectionLongitude(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid);

    /**
     * @brief computePressureCoordinateTransformationLatitude Computes pressure
     * correction terms for all data on irregular vertical pressure coordinates.
     * This term corrects the derivatives on the native data grid to derivatives on pressure level.
     * @param inputGrid pointer to the input grid
     * @param resultGrid pointer to the result grid
     */
    void computePressureCoordinateCorrectionLatitude(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid);

    /**
     * @brief periodicBoundaryTreatment Test if periodic boundaries are present.
     * @param inputGrid pointer to the input grid.
     * @return QList of 3 boolean:
     *          index 0: longitudinal periodic boundary conditions,
     *          index 1: periodic boundary conditions at north pole,
     *          index 2: periodic boundary conditions at south pole
     */
    QList<bool> periodicBoundaryTreatment(MStructuredGrid *inputGrid);

    /**
     * @brief callLibcalvarPartialDerivativeRoutine function interface to call the
     * ddh3 routine of the libcalvar LAGRANTO routine.
     * @param inputGrid pointer to the input grid
     * @param resultGrid pointer to the result grid
     * @param direction horizontal direction of the partial derivatives ("lon" or "lat")
     */
    void callLibcalvarPartialDerivativeRoutine(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid,
            QString direction);

};

} // namespace Met3D

#endif // PARTIALDERIVATIVEFILTER_H
